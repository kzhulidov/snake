snake: snake.c
	$(CC) -O3 snake.c -o snake -lcurses

clean:
	$(RM) snake

.PHONY: clean
