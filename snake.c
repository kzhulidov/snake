#include <curses.h>
#include <signal.h>
#include <termios.h>
#include <unistd.h>   // for read()
#include <string.h>

static struct termios initial_settings, new_settings;
static char peek_character = -1;
static int b_running = 1;

static void finish(int sig);
static void init_keyboard();
static void close_keyboard();
static int kbhit();
static int readch();

typedef struct {
  int x;
  int y;
} t_pos;

int main()
{
  int x_inc = 0;
  int y_inc = -1;
  t_pos cell[32];
  int len = 8;
  
  memset(cell, 0, sizeof(cell));
  
  signal(SIGINT, finish);
  initscr();
  nonl();
  cbreak();

  init_keyboard();

  // initial pos
  cell[0].x = 40;
  cell[0].y = 20;

  while (b_running)
  {
    int x;
    int y;
    int i;
    
    // read keys
    if (kbhit())
    {
      switch (readch())
      {
        case 'Q':
        case 'q':
        {
          b_running = 0;
          break;
        }
        case 'w':
        {
          y_inc = -1;
          x_inc = 0;
          break;
        }
        case 's':
        {
          y_inc = 1;
          x_inc = 0;
          break;
        }
        case 'a':
        {
          x_inc = -1;
          y_inc = 0;
          break;
        }
        case 'd':
        {
          x_inc = 1;
          y_inc = 0;
          break;
        }
      }
    }
    
    // calc new pos
    x = cell[0].x + x_inc;
    y = cell[0].y + y_inc;
    
    // draw
    //printf("erase at: %d, %d\n", cell[len - 1].x, cell[len - 1].y);
    //printf(" draw at: %d, %d\n", x, y);
    mvaddch(cell[len - 1].y, cell[len - 1].x, ' ');
    mvaddch(y, x, 'o');
    refresh();
    
    // shift cells
    for (i = len - 1; i > 0; i--) {
      cell[i] = cell[i - 1];
    }
    cell[0].x = x;
    cell[0].y = y;
    
    sleep(1);
  }

  close_keyboard();

  endwin();
  return 0;
}

static void finish(int sig)
{
  b_running = 0;
}

static void init_keyboard()
{
    tcgetattr(0,&initial_settings);
    new_settings = initial_settings;
    new_settings.c_lflag &= ~ICANON;
    new_settings.c_lflag &= ~ECHO;
    new_settings.c_lflag &= ~ISIG;
    new_settings.c_cc[VMIN] = 1;
    new_settings.c_cc[VTIME] = 0;
    tcsetattr(0, TCSANOW, &new_settings);
}

static void close_keyboard()
{
    tcsetattr(0, TCSANOW, &initial_settings);
}

static int kbhit()
{
char ch;
int nread;

    if (peek_character != -1) return 1;
    new_settings.c_cc[VMIN]=0;
    tcsetattr(0, TCSANOW, &new_settings);
    nread = read(0,&ch,1);
    new_settings.c_cc[VMIN]=1;
    tcsetattr(0, TCSANOW, &new_settings);
    if(nread == 1)
    {
        peek_character = ch;
        return 1;
    }
    return 0;
}

static int readch()
{
char ch;

    if(peek_character != -1)
    {
        ch = peek_character;
        peek_character = -1;
        return ch;
    }
    read(0,&ch,1);
    return ch;
}

